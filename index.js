class Burger {
  constructor(size, stuffing, dressing) {
    this.size = size;
    this.stuffing = stuffing;
    this.dressing = dressing;
    try {
      try {
        if (!this.burgerSize[size]) {
          const e = new Error("cannot create new burger with desired size");
          e.name = "BurgerSizeError";
          throw e;
        }
        if (!this.stuffingTypes[stuffing]) {
          const e = new Error("cannot create new burger with desired stuffing");
          e.name = "StuffingError";
          throw e;
        }
        if (!this.dressingTypes[dressing]) {
          const e = new Error("cannot create new burger with desired dressing");
          e.name = "DressingError";
          throw e;
        }
        this.size = size;
        this.stuffing = stuffing;
        this.dressing = dressing;
      } catch (err) {
        if (err.name == "BurgerSizeError") {
          console.log("sizeErr: " + err);
        }
        if (err.name == "StuffingError") {
          console.log("stuffErr: " + err);
        }
        if (err.name == "DressingError") {
          console.log("dressErr: " + err);
        } else {
          throw err;
        }
      }
    } catch (err) {
      console.log("otherErr: " + err);
    }
  }
  checkASize() {
    return this.size;
  }
  findStuffing() {
    return this.stuffing;
  }
  findDressing() {
    return this.dressing;
  }
  deleteASize() {
    return (this.size = 0);
  }
  //------------------------methods-to-change--size-------------------------------
  orderSmall() {
    console.log(
      `Previous choice -  ${this.checkASize()} size, changed to small`
    );
    this.burgerSize[this.checkASize()]["calories"] = 0;
    this.burgerSize[this.checkASize()]["price"] = 0;
    return (this.size = "small");
  }
  orderLarge() {
    console.log(
      `Previous choice -  ${this.checkASize()} size, changed to large`
    );
    this.burgerSize[this.checkASize()]["calories"] = 0;
    this.burgerSize[this.checkASize()]["price"] = 0;
    return (this.size = "large");
  }

  seeSize() {
    return this.size;
  }
  //------------------------methods-to-change--dressing-----------------------------
  sauceDressing() {
    console.log(`Will add sauce dressing`);
    this.dressingTypes[this.findDressing()] = this.dressingTypes.sauce;
    return (this.dressing = "sauce");
  }

  mayoDressing() {
    console.log(`Will add mayo dressing`);
    this.dressingTypes[this.findDressing()] = this.dressingTypes.mayo;
    return (this.dressing = "mayo");
  }

  noDressing() {
    console.log(`Will take away any dressing`);
    this.dressingTypes[this.findDressing()]["price"] = 0;
    this.dressingTypes[this.findDressing()]["calories"] = 0;
    return (this.dressing = "no dressing");
  }

  lookAtDressing() {
    return this.dressing;
  }
  //------------------------methods-to-change--stuffing-----------------------------
  cheeseStuffing() {
    if (this.stuffingTypes[this.findStuffing()] === this.stuffingTypes.cheese) {
      return "You should choose another stuffing";
    }
    this.stuffingTypes[this.findStuffing()] = this.stuffingTypes.cheese;
    return (this.stuffing = "cheese");
  }

  potatoStuffing() {
    if (this.stuffingTypes[this.findStuffing()] === this.stuffingTypes.potato) {
      return "You should choose another stuffing";
    }
    this.stuffingTypes[this.findStuffing()] = this.stuffingTypes.potato;
    return (this.stuffing = "potato");
  }

  saladStuffing() {
    if (this.stuffingTypes[this.findStuffing()] === this.stuffingTypes.salad) {
      return "You should choose another stuffing";
    }
    this.stuffingTypes[this.findStuffing()] = this.stuffingTypes.salad;
    return (this.stuffing = "salad");
  }

  lookAtStuffing() {
    return this.stuffing;
  }

  //---------------------calculate--burger--price--------------------------------
  static staticPrice(burger) {
    if (burger.dressingTypes[burger.findDressing] !== undefined) {
      return (
        burger.burgerSize[burger.checkASize()]["price"] +
        burger.stuffingTypes[burger.findStuffing()]["price"] +
        burger.dressingTypes[burger.findDressing()]["price"]
      );
    }
    return (
      burger.burgerSize[burger.checkASize()]["price"] +
      burger.stuffingTypes[burger.findStuffing()]["price"]
    );
  }
  //---------------------calculate--burger--calories------------------------------
  static staticFat(burger) {
    if (burger.dressingTypes[burger.findDressing] !== undefined) {
      return (
        burger.burgerSize[burger.checkASize()]["calories"] +
        burger.stuffingTypes[burger.findStuffing()]["calories"] +
        burger.dressingTypes[burger.findDressing()]["calories"]
      );
    }
    return (
      burger.burgerSize[burger.checkASize()]["calories"] +
      burger.stuffingTypes[burger.findStuffing()]["calories"]
    );
  }
}

Burger.prototype.burgerSize = {
  small: {
    price: 50,
    calories: 20
  },
  large: {
    price: 100,
    calories: 40
  }
};

Burger.prototype.stuffingTypes = {
  cheese: {
    calories: 20,
    price: 10
  },
  salad: {
    calories: 5,
    price: 20
  },
  potato: {
    calories: 10,
    price: 15
  }
};
Burger.prototype.dressingTypes = {
  sauce: {
    calories: 0,
    price: 15
  },
  mayo: {
    calories: 5,
    price: 20
  }
};

//-------------------------testing---------------------------------------------
console.log(Burger.prototype.stuffingTypes);
console.log(Burger.prototype.burgerSize);
console.log(Burger.prototype.dressingTypes);

let smallBurger = new Burger("small", "salad", "mayo");
let largeBurger = new Burger("large", "cheese", "sauce");
let noSauce = new Burger("small", "potato");
let strangeBurger = new Burger("medium", "mushrom", "tai");

console.log(Burger.staticPrice(noSauce));
console.log(Burger.staticFat(noSauce));

console.log(Burger.staticPrice(smallBurger));
console.log(Burger.staticPrice(largeBurger));
console.log(Burger.staticFat(smallBurger));
console.log(Burger.staticFat(largeBurger));
console.log(largeBurger.findDressing());

console.log(largeBurger.checkASize());

console.log(largeBurger.orderSmall());

console.log(largeBurger.orderLarge());

console.log(largeBurger);

console.log(smallBurger.cheeseStuffing());
console.log(smallBurger.potatoStuffing());
console.log(smallBurger.noDressing());
console.log(smallBurger);
console.log(Burger.staticPrice(smallBurger));
console.log(Burger.staticFat(largeBurger));
console.log(smallBurger.lookAtStuffing());
console.log(smallBurger.hasOwnProperty("size"));

console.log(smallBurger.potatoStuffing());
console.log(smallBurger.saladStuffing());
console.log(smallBurger.cheeseStuffing());
